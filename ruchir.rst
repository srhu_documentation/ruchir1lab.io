ruchir pandey
=============

Alternate Domains
=================

Read the Docs supports a number of custom domains for your convenience. Shorter urls make everyone happy, and we like making people happy!

Subdomain Support
------------------

Every project has a subdomain that is available to serve its documentation. If you go to <slug>.readthedocs.io, it should show you the latest version of documentation. A good example is http://pip.readthedocs.io

.. note:: If you have an old project that has an underscore (_) in the name, it will use a subdomain with a hyphen (-).
          `RFC 1035 <http://tools.ietf.org/html/rfc1035>`_ has more information on valid subdomains.

CNAME Support
-------------

If you have your own domain, you can still host with us.
This requires two steps:

* Add a CNAME record in your DNS that point to our servers `readthedocs.io`
* Add a Domain object in the **Project Admin > Domains** page for your project.

.. note:: The ``Domain`` that should be used is the actual subdomain that you want your docs served on.
          Generally it will be `docs.projectname.org`.

Using pip as an example, http://www.pip-installer.org resolves, but is hosted on our infrastructure.    